function map(arr,cb){
    const array=[]
    if(arr.length>0){
        for (let index=0;index<arr.length;index++){
            const result=cb(arr[index],index)
            array.push(result)

            
        }
        return array
    }
    else{
        return "empty array"
    }

}

function logic(item,index){
    return [item,index]
}

module.exports={map,logic}